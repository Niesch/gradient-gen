package gradient

import (
	"errors"
	"fmt"
	"image"
	"image/color"
	"strconv"
	"strings"
)

type GradientImage struct {
	Width       int
	Height      int
	FromColor   color.RGBA
	ToColor     color.RGBA
	LeftToRight bool
}

func (g *GradientImage) Generate() *image.RGBA {
	img := image.NewRGBA(image.Rect(0, 0, g.Width, g.Height))
	if g.LeftToRight {
		for row := 0; row < g.Height; row++ {
			percentage := (float64(row) / float64(g.Height))
			red := uint8(float64(g.FromColor.R)*(1.0-percentage)) + uint8(float64(g.ToColor.R)*(percentage))
			green := uint8(float64(g.FromColor.G)*(1.0-percentage)) + uint8(float64(g.ToColor.G)*(percentage))
			blue := uint8(float64(g.FromColor.B)*(1.0-percentage)) + uint8(float64(g.ToColor.B)*(percentage))
			alpha := uint8(float64(g.FromColor.A)*(1.0-percentage)) + uint8(float64(g.ToColor.A)*(percentage))
			fmt.Printf("%f: Writing column %d with rgba values %d, %d, %d, %d in LTR mode. \n", percentage, row, red, green, blue, alpha)
			for column := 0; column < g.Width; column++ {
				img.Set(row, column, color.RGBA{red, green, blue, alpha})
			}

		}
	} else {
		for column := 0; column < g.Width; column++ {
			percentage := (float64(column) / float64(g.Width))
			red := uint8(float64(g.FromColor.R)*(1.0-percentage)) + uint8(float64(g.ToColor.R)*(percentage))
			green := uint8(float64(g.FromColor.G)*(1.0-percentage)) + uint8(float64(g.ToColor.G)*(percentage))
			blue := uint8(float64(g.FromColor.B)*(1.0-percentage)) + uint8(float64(g.ToColor.B)*(percentage))
			alpha := uint8(float64(g.FromColor.A)*(1.0-percentage)) + uint8(float64(g.ToColor.A)*(percentage))
			fmt.Printf("%f: Writing row %d with rgba values %d, %d, %d, %d in TTB mode.\n", percentage, column, red, green, blue, alpha)
			for row := 0; row < g.Height; row++ {
				img.Set(row, column, color.RGBA{red, green, blue, alpha})
			}
		}
	}
	return img
}

func Create(w, h int) *GradientImage {
	return &GradientImage{Width: w, Height: h}
}

func ParseColor(in string) (*color.RGBA, error) {
	s := strings.Split(in, ",")
	if len(s) != 4 {
		return nil, errors.New("wrong amount of parameters")
	}
	c := make([]uint8, 4)
	for i, _ := range s {
		t, err := strconv.Atoi(s[i])
		if err != nil {
			return nil, err
		}
		if 0 > t || t > 255 {
			return nil, errors.New(fmt.Sprintf("%d is out of range 0-255", t))
		}
		c[i] = uint8(t)
	}
	return &color.RGBA{c[0], c[1], c[2], c[3]}, nil
}
