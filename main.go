package main

import (
	"flag"
	"fmt"
	"gitlab.com/Niesch/gradient-gen/gradient"
	"image/png"
	"os"
)

func main() {
	var w, h int
	var from, to string
	var ltr, license bool
	var err error
	flag.IntVar(&w, "w", 100, "the width of the image")
	flag.IntVar(&h, "h", 100, "the width of the image")
	flag.StringVar(&from, "from", "255,0,0,255", "the colour to gradient from, in comma-separated RGBA")
	flag.StringVar(&to, "to", "0,255,0,0", "the colour to gradient to, in comma-separated RGBA")
	flag.BoolVar(&ltr, "ltr", false, "whether the gradient runs left-to-right")
	flag.BoolVar(&license, "license", false, "show license information")
	flag.Parse()

	if license {
		fmt.Println(`    gradient-gen
    Copyright (C) 2016  Niesch

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.`)
		os.Exit(0)
	}

	img := gradient.Create(w, h)

	img.LeftToRight = ltr
	// parse colours from the users strings
	frc, err := gradient.ParseColor(from)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	toc, err := gradient.ParseColor(to)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	img.ToColor, img.FromColor = *toc, *frc
	res := img.Generate()

	f, err := os.OpenFile("out.png", os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	defer f.Close()
	png.Encode(f, res)
}
